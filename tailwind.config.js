module.exports = {
  theme: {
    extend: {
      colors: {
        bark: {
          100: '#40beb5',
          200: '#368781',
          300: '#236863',
          400: '#144a46',
          500: '#092b28',
        },
      },
    },
    fontSize: {
      xs: '0.75rem',
      sm: '0.875rem',
      base: '1rem',
      lg: '1.125rem',
      xl: '1.25rem',
      '2x': '1.5rem',
      '3x': '1.875rem',
      '4x': '2.25rem',
      '5x': '3rem',
      '6x': '4rem',
      huge: '5rem',
      '7x': '6rem',
    },
    letterSpacing: {
      tightest: '-.0625em',
      tighter: '-.05em',
      tight: '-.025em',
      normal: '0',
      wide: '.025em',
      wider: '.05em',
      widest: '.75em',
    },
  },
  variants: {
    backgroundColor: ['responsive', 'hover', 'focus', 'active'],
    fontSize: ['responsive', 'hover', 'focus'],
  },
  plugins: [],
}
