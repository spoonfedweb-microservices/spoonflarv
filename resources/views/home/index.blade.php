@extends('common.layout')

@section('content')
<div class="flex-center position-ref full-height">
  <div class="content">
    <div class="title text-7x text-gray-900">
      spoonfedweb
    </div>
    <div class="uppercase tracking-wide no-underline">
      <a class="px-4 py-2 text-blue-900 hover:bg-bark-400 hover:text-white rounded-full"
        href="https://gitlab.com/spoonfedweb-microservices/spoonflarv" target="_blank">Gitlab</a>
    </div>
  </div>
</div>
@endsection
